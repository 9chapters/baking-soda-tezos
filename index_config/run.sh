#!/bin/bash

mkdir -p log

PAGE=${1:-0}
DOC_PER_PAGE=${2:-3000}
RPC=${3:-https://rpc.tezrpc.me}
CORE=${4:-1}
CONN_STR=${5:-"host=0.0.0.0 dbname=tezos user=postgres port=5433"}
CONN_STR_READ=${6:-${CONN_STR}}

#
# run tezos_block crawler
#

nohup stack exec -- baking-soda-tezos Crawl -u $RPC -c "$CONN_STR" -r "$CONN_STR_READ" -i tezos -n 500 -t 500 +RTS -N${CORE} -RTS >./log/tezos.crawler.log 2>&1  &

#
# run mytezosbaker crawler
#

nohup stack exec -- baking-soda-tezos Crawl -c "$CONN_STR" -r "$CONN_STR_READ" -i mytezosbaker -n 1 -t 1 +RTS -N${CORE} -RTS >./log/mytezosbaker.crawler.log 2>&1  &

#
# run tuliptoolstotal crawler
#

nohup stack exec -- baking-soda-tezos Crawl -c "$CONN_STR" -r "$CONN_STR_READ" -i tuliptoolstotal -n 1 -t 1 +RTS -N${CORE} -RTS >./log/tuliptoolstotal.crawler.log 2>&1  &

#
# run tuliptoolscirculating crawler
#

nohup stack exec -- baking-soda-tezos Crawl -c "$CONN_STR" -r "$CONN_STR_READ" -i tuliptoolscirculating -n 1 -t 1 +RTS -N${CORE} -RTS >./log/tuliptoolscirculating.crawler.log 2>&1  &

sleep 60;

#
# run tezos_block parser for indexing
#

for i in `ls -1 ./tezos_block/prod/config.*`
do

  filename=`basename $i`
  nohup stack exec -- baking-soda-tezos Parser -e ./tezos_block/model -c "$CONN_STR" -r "$CONN_STR_READ" -i tezos -f $i -p ${PAGE} -n ${DOC_PER_PAGE} -t ${DOC_PER_PAGE} +RTS -N${CORE} -RTS  >./log/${filename}.parser.log 2>&1  &

done

#
# run mytezosbaker parser for indexing
#
nohup stack exec -- baking-soda-tezos Parser -e ./mytezosbaker/model -c "$CONN_STR" -r "$CONN_STR_READ" -i mytezosbaker -f ./mytezosbaker/config -n 1 -t 1000 +RTS -N${CORE} -RTS  >./log/mytezosbaker.parser.log 2>&1  &

#
# run tuliptoolstotal parser for indexing
#
nohup stack exec -- baking-soda-tezos Parser -e ./tuliptools_total/model -c "$CONN_STR" -r "$CONN_STR_READ" -i tuliptoolstotal -f ./tuliptools_total/config -n 1 -t 1000 +RTS -N${CORE} -RTS  >./log/tuliptoolstotal.parser.log 2>&1  &

#
# run tuliptoolscirculating parser for indexing
#
nohup stack exec -- baking-soda-tezos Parser -e ./tuliptools_circulating/model -c "$CONN_STR" -r "$CONN_STR_READ" -i tuliptoolscirculating -f ./tuliptools_circulating/config -n 1 -t 1000 +RTS -N${CORE} -RTS  >./log/tuliptoolscirculating.parser.log 2>&1  &

#
# postprocess process
#

nohup stack exec -- baking-soda-tezos -c "$CONN_STR" -r "$CONN_STR_READ" PostprocessDB +RTS -N2 -RTS >./log/tezos.postprocess.log 2>&1  &
