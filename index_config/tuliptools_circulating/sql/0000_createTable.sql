CREATE TABLE tulip_tools_circulating (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "circulating"          float8,
    "uuid"                 character varying unique);
CREATE INDEX "tulip_tools_circulating_circulating_idx" ON tulip_tools_circulating ("circulating");
CREATE INDEX "tulip_tools_circulating_uuid_idx" ON tulip_tools_circulating ("uuid");

