#!/bin/bash

GREP=${1:-grep}

head -1                             config  > ./prod/config.blocks.ops.op_txs
head -1                             config  > ./prod/config.others
head -1                             config  > ./prod/config.balances.1
head -1                             config  > ./prod/config.balances.2
head -1                             config  > ./prod/config.op_originations.op_endorsements


$GREP " ops "             config >> ./prod/config.blocks.ops.op_txs
$GREP " block_alphas "    config >> ./prod/config.blocks.ops.op_txs
$GREP " blocks "          config >> ./prod/config.blocks.ops.op_txs
$GREP " op_txs "          config >> ./prod/config.blocks.ops.op_txs

$GREP " balances "                   config | $GREP -P -v " \[Group:\d\d\d" >> ./prod/config.balances.1
$GREP " balances "                   config | $GREP -P " \[Group:\d\d\d" >> ./prod/config.balances.2

$GREP " op_endorsements " config >> ./prod/config.op_originations.op_endorsements
$GREP " op_originations "            config >> ./prod/config.op_originations.op_endorsements

$GREP " op_reveals "                 config >> ./prod/config.others
$GREP " op_delegations "             config >> ./prod/config.others
$GREP " op_activate_accounts "       config >> ./prod/config.others
$GREP " op_seed_nonce_revelations "  config >> ./prod/config.others
$GREP " internal_op_txs "            config >> ./prod/config.others
$GREP " internal_op_delegations "    config >> ./prod/config.others
$GREP " op_ballots "                 config >> ./prod/config.others
$GREP " op_proposals "               config >> ./prod/config.others
$GREP " bh2s "                       config >> ./prod/config.others
$GREP " bh1s "                       config >> ./prod/config.others
$GREP " op2s "                       config >> ./prod/config.others
$GREP " op1s "                       config >> ./prod/config.others
$GREP " op_double_baking_evidences " config >> ./prod/config.others
$GREP " op_double_endorsement_evidences " config >> ./prod/config.others
$GREP " accounts "                        config >> ./prod/config.others
