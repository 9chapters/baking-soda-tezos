--
-- To create a view for easy getting data
--
-- knewn bug: ERROR:  column "xxx" specified more than once
-- fix: need specified column
--


create view block_balances as
  select
    blocks.*,
    block_alphas.*,
    balances.*
    baker.baker_name
  from blocks
  inner join block_alphas on blocks.uuid = block_alphas.block_uuid
  inner join block_alphas on blocks.baker = baker.baker_name
  inner join balances     on block_alphas.uuid = balances.parent_uuid
;

create view block_op as
  select
    blocks.*,
    ops.*
  from blocks
  inner join block_alphas on blocks.uuid = ops.block_uuid
;

create view tx_fulls as
  select
    ops.*,
    op_txs.*
    internal_op_txs.*
  from ops
  inner join op_txs on ops.uuid = op_txs.op_uuid
  inner join balances m on balances.parent_uuid = op_txs.metadata_uuid
  inner join balances o on balances.parent_uuid = op_txs.operation_result_uuid
  inner join internal_op_txs i on internal_op_txs.op_metadata_uuid = op_txs.metadata_uuid;
;

create view internal_txs_full as
  select
    internal_op_txs.*
    balances.*
  from internal_op_txs
  inner join on balances.parent_uuid = internal_op_txs.result_uuid
;

create view proposals_fulls as
  select
    ops.*,
    op_proposals.*
  from ops
  inner join op_proposals on op_proposals.uuid = ops.op_uuid
  inner join balances m on balances.parent_uuid = op_proposals.metadata_uuid
--  inner join balances o on balances.parent_uuid = op_proposals.operation_result_uuid
;

create view reveals_fulls as
  select
    ops.*,
    op_reveals.*
  from ops
  inner join op_reveals on op_reveals.uuid = ops.op_uuid
  inner join balances m on balances.parent_uuid = op_reveals.metadata_uuid
  inner join balances o on balances.parent_uuid = op_reveals.operation_result_uuid
;

create view delegation_fulls as
  select
    ops.*,
    op_delegations.*
  from ops
  inner join op_delegations on op_delegations.uuid = ops.op_uuid
  inner join balances m on balances.parent_uuid = op_delegations.metadata_uuid
  inner join balances o on balances.parent_uuid = op_delegations.operation_result_uuid
;

create view origination_fulls as
  select
    ops.*,
    op_originations.*
  from ops
  inner join op_originations on op_originations.uuid = ops.op_uuid
  inner join balances m on balances.parent_uuid = op_originations.metadata_uuid
  inner join balances o on balances.parent_uuid = op_originations.operation_result_uuid;
;

create view double_baking_evidence_fulls as
  select
    ops.*,
    op_double_baking_evidence.*
  from ops
  inner join op_double_baking_evidence on op_double_baking_evidence.uuid = ops.op_uuid
  inner join bh1s h1 on bh1s.double_baking_evidence_uuid = op_double_baking_evidence.uuid
  inner join bh2s h2 on bh2s.double_baking_evidence_uuid = op_double_baking_evidence.uuid
  inner join balances m on balances.parent_uuid = op_double_baking_evidence.metadata_uuid
--  inner join balances o on balances.parent_uuid = op_double_baking_evidence.operation_result_uuid
;


create view op_seed_nonce_revelationfulls as
  select
    ops.*,
    op_seed_nonce_revelation.*
  from ops
  inner join op_seed_nonce_revelation on op_seed_nonce_revelation.uuid = ops.op_uuid
  inner join balances m on balances.parent_uuid = op_seed_nonce_revelation.metadata_uuid
--  inner join balances o on balances.parent_uuid = op_seed_nonce_revelation.operation_result_uuid
;

create view endorment_fulls as
  select
    ops.*,
    op_endorment.*
  from ops
  inner join op_endorment on op_endorment.uuid = ops.op_uuid
  inner join balances m on balances.parent_uuid = op_endorment.metadata_uuid
--  inner join balances o on balances.parent_uuid = op_endorment.operation_result_uuid
;

create view ballot_fulls as
  select
    ops.*,
    op_ballot.*
  from ops
  inner join op_ballot on op_ballot.uuid = ops.op_uuid
  inner join balances m on balances.parent_uuid = op_ballot.metadata_uuid
--  inner join balances o on balances.parent_uuid = op_ballot.operation_result_uuid
;
