CREATE TABLE op_activate_accounts (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"    float8,
    "block_hash"           character varying,
    "block_level"          float8,
    "block_timestamp"      character varying,
    "kind"                 character varying,
    "metadata_uuid"        character varying,
    "op_uuid"              character varying,
    "pkh"                  character varying,
    "secret"               character varying,
    "uuid"                 character varying unique);
CREATE INDEX "op_activate_accounts_block_alpha_cycle_idx" ON op_activate_accounts ("block_alpha_cycle");
CREATE INDEX "op_activate_accounts_block_hash_idx" ON op_activate_accounts ("block_hash");
CREATE INDEX "op_activate_accounts_block_level_idx" ON op_activate_accounts ("block_level");
CREATE INDEX "op_activate_accounts_block_timestamp_idx" ON op_activate_accounts ("block_timestamp");
CREATE INDEX "op_activate_accounts_kind_idx" ON op_activate_accounts ("kind");
CREATE INDEX "op_activate_accounts_metadata_uuid_idx" ON op_activate_accounts ("metadata_uuid");
CREATE INDEX "op_activate_accounts_op_uuid_idx" ON op_activate_accounts ("op_uuid");
CREATE INDEX "op_activate_accounts_pkh_idx" ON op_activate_accounts ("pkh");
CREATE INDEX "op_activate_accounts_secret_idx" ON op_activate_accounts ("secret");
CREATE INDEX "op_activate_accounts_uuid_idx" ON op_activate_accounts ("uuid");

CREATE TABLE ops (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"    float8,
    "block_hash"           character varying,
    "block_level"          float8,
    "block_timestamp"      character varying,
    "block_uuid"           character varying,
    "branch"               character varying,
    "chain_id"             character varying,
    "op_hash"              character varying,
    "protocol"             character varying,
    "signature"            character varying,
    "uuid"                 character varying unique);
CREATE INDEX "ops_block_alpha_cycle_idx" ON ops ("block_alpha_cycle");
CREATE INDEX "ops_block_hash_idx" ON ops ("block_hash");
CREATE INDEX "ops_block_level_idx" ON ops ("block_level");
CREATE INDEX "ops_block_timestamp_idx" ON ops ("block_timestamp");
CREATE INDEX "ops_block_uuid_idx" ON ops ("block_uuid");
CREATE INDEX "ops_branch_idx" ON ops ("branch");
CREATE INDEX "ops_chain_id_idx" ON ops ("chain_id");
CREATE INDEX "ops_op_hash_idx" ON ops ("op_hash");
CREATE INDEX "ops_protocol_idx" ON ops ("protocol");
CREATE INDEX "ops_signature_idx" ON ops ("signature");
CREATE INDEX "ops_uuid_idx" ON ops ("uuid");

CREATE TABLE op_proposals (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"    float8,
    "block_hash"           character varying,
    "block_level"          float8,
    "block_timestamp"      character varying,
    "kind"                 character varying,
    "metadata_uuid"        character varying,
    "op_uuid"              character varying,
    "period"               float8,
    "proposals"            character varying[]
       DEFAULT '{}',
    "source"               character varying,
    "uuid"                 character varying unique);
CREATE INDEX "op_proposals_block_alpha_cycle_idx" ON op_proposals ("block_alpha_cycle");
CREATE INDEX "op_proposals_block_hash_idx" ON op_proposals ("block_hash");
CREATE INDEX "op_proposals_block_level_idx" ON op_proposals ("block_level");
CREATE INDEX "op_proposals_block_timestamp_idx" ON op_proposals ("block_timestamp");
CREATE INDEX "op_proposals_kind_idx" ON op_proposals ("kind");
CREATE INDEX "op_proposals_metadata_uuid_idx" ON op_proposals ("metadata_uuid");
CREATE INDEX "op_proposals_op_uuid_idx" ON op_proposals ("op_uuid");
CREATE INDEX "op_proposals_period_idx" ON op_proposals ("period");
CREATE INDEX "op_proposals_proposals_idx" ON op_proposals ("proposals");
CREATE INDEX "op_proposals_source_idx" ON op_proposals ("source");
CREATE INDEX "op_proposals_uuid_idx" ON op_proposals ("uuid");

CREATE TABLE blocks (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "chain_id"             character varying,
    "context"              character varying,
    "fitness"              character varying[]
       DEFAULT '{}',
    "hash"                 character varying,
    "level"                float8,
    "operations_hash"      character varying,
    "predecessor"          character varying,
    "priority"             float8,
    "proof_of_work_nonce"  character varying,
    "proto"                float8,
    "protocol"             character varying,
    "seed_nonce_hash"      character varying,
    "signature"            character varying,
    "timestamp"            character varying,
    "uuid"                 character varying unique,
    "validation_pass"      float8);
CREATE INDEX "blocks_chain_id_idx" ON blocks ("chain_id");
CREATE INDEX "blocks_context_idx" ON blocks ("context");
CREATE INDEX "blocks_fitness_idx" ON blocks ("fitness");
CREATE INDEX "blocks_hash_idx" ON blocks ("hash");
CREATE INDEX "blocks_level_idx" ON blocks ("level");
CREATE INDEX "blocks_operations_hash_idx" ON blocks ("operations_hash");
CREATE INDEX "blocks_predecessor_idx" ON blocks ("predecessor");
CREATE INDEX "blocks_priority_idx" ON blocks ("priority");
CREATE INDEX "blocks_proof_of_work_nonce_idx" ON blocks ("proof_of_work_nonce");
CREATE INDEX "blocks_proto_idx" ON blocks ("proto");
CREATE INDEX "blocks_protocol_idx" ON blocks ("protocol");
CREATE INDEX "blocks_seed_nonce_hash_idx" ON blocks ("seed_nonce_hash");
CREATE INDEX "blocks_signature_idx" ON blocks ("signature");
CREATE INDEX "blocks_timestamp_idx" ON blocks ("timestamp");
CREATE INDEX "blocks_uuid_idx" ON blocks ("uuid");
CREATE INDEX "blocks_validation_pass_idx" ON blocks ("validation_pass");

CREATE TABLE op_reveals (
    "_inserted_timestamp"            timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"              float8,
    "block_hash"                     character varying,
    "block_level"                    float8,
    "block_timestamp"                character varying,
    "counter"                        character varying,
    "fee"                            character varying,
    "gas_limit"                      character varying,
    "kind"                           character varying,
    "metadata_uuid"                  character varying,
    "op_uuid"                        character varying,
    "operation_result_consumed_gas"  character varying,
    "operation_result_errors"        character varying,
    "operation_result_status"        character varying,
    "operation_result_uuid"          character varying,
    "public_key"                     character varying,
    "source"                         character varying,
    "storage_limit"                  character varying,
    "uuid"                           character varying unique);
CREATE INDEX "op_reveals_block_alpha_cycle_idx" ON op_reveals ("block_alpha_cycle");
CREATE INDEX "op_reveals_block_hash_idx" ON op_reveals ("block_hash");
CREATE INDEX "op_reveals_block_level_idx" ON op_reveals ("block_level");
CREATE INDEX "op_reveals_block_timestamp_idx" ON op_reveals ("block_timestamp");
CREATE INDEX "op_reveals_counter_idx" ON op_reveals ("counter");
CREATE INDEX "op_reveals_fee_idx" ON op_reveals ("fee");
CREATE INDEX "op_reveals_gas_limit_idx" ON op_reveals ("gas_limit");
CREATE INDEX "op_reveals_kind_idx" ON op_reveals ("kind");
CREATE INDEX "op_reveals_metadata_uuid_idx" ON op_reveals ("metadata_uuid");
CREATE INDEX "op_reveals_op_uuid_idx" ON op_reveals ("op_uuid");
CREATE INDEX "op_reveals_operation_result_consumed_gas_idx" ON op_reveals ("operation_result_consumed_gas");
CREATE INDEX "op_reveals_operation_result_status_idx" ON op_reveals ("operation_result_status");
CREATE INDEX "op_reveals_operation_result_uuid_idx" ON op_reveals ("operation_result_uuid");
CREATE INDEX "op_reveals_public_key_idx" ON op_reveals ("public_key");
CREATE INDEX "op_reveals_source_idx" ON op_reveals ("source");
CREATE INDEX "op_reveals_storage_limit_idx" ON op_reveals ("storage_limit");
CREATE INDEX "op_reveals_uuid_idx" ON op_reveals ("uuid");

CREATE TABLE accounts (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "account"              character varying unique);
CREATE INDEX "accounts_account_idx" ON accounts ("account");

CREATE TABLE balances (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"    float8,
    "block_hash"           character varying,
    "block_level"          float8,
    "block_timestamp"      character varying,
    "category"             character varying,
    "change"               character varying,
    "contract"             character varying,
    "cycle"                float8,
    "delegate"             character varying,
    "kind"                 character varying,
    "op_kind"              character varying,
    "parent_uuid"          character varying,
    "uuid"                 character varying unique);
CREATE INDEX "balances_block_alpha_cycle_idx" ON balances ("block_alpha_cycle");
CREATE INDEX "balances_block_hash_idx" ON balances ("block_hash");
CREATE INDEX "balances_block_level_idx" ON balances ("block_level");
CREATE INDEX "balances_block_timestamp_idx" ON balances ("block_timestamp");
CREATE INDEX "balances_category_idx" ON balances ("category");
CREATE INDEX "balances_change_idx" ON balances ("change");
CREATE INDEX "balances_contract_idx" ON balances ("contract");
CREATE INDEX "balances_cycle_idx" ON balances ("cycle");
CREATE INDEX "balances_delegate_idx" ON balances ("delegate");
CREATE INDEX "balances_kind_idx" ON balances ("kind");
CREATE INDEX "balances_op_kind_idx" ON balances ("op_kind");
CREATE INDEX "balances_parent_uuid_idx" ON balances ("parent_uuid");
CREATE INDEX "balances_uuid_idx" ON balances ("uuid");

CREATE TABLE op_endorsements (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"    float8,
    "block_hash"           character varying,
    "block_level"          float8,
    "block_timestamp"      character varying,
    "delegate"             character varying,
    "kind"                 character varying,
    "level"                float8,
    "metadata_uuid"        character varying,
    "op_uuid"              character varying,
    "slots"                float8[]
       DEFAULT '{}',
    "uuid"                 character varying unique);
CREATE INDEX "op_endorsements_block_alpha_cycle_idx" ON op_endorsements ("block_alpha_cycle");
CREATE INDEX "op_endorsements_block_hash_idx" ON op_endorsements ("block_hash");
CREATE INDEX "op_endorsements_block_level_idx" ON op_endorsements ("block_level");
CREATE INDEX "op_endorsements_block_timestamp_idx" ON op_endorsements ("block_timestamp");
CREATE INDEX "op_endorsements_delegate_idx" ON op_endorsements ("delegate");
CREATE INDEX "op_endorsements_kind_idx" ON op_endorsements ("kind");
CREATE INDEX "op_endorsements_level_idx" ON op_endorsements ("level");
CREATE INDEX "op_endorsements_metadata_uuid_idx" ON op_endorsements ("metadata_uuid");
CREATE INDEX "op_endorsements_op_uuid_idx" ON op_endorsements ("op_uuid");
CREATE INDEX "op_endorsements_slots_idx" ON op_endorsements ("slots");
CREATE INDEX "op_endorsements_uuid_idx" ON op_endorsements ("uuid");

CREATE TABLE op1s (
    "_inserted_timestamp"                timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"                  float8,
    "block_hash"                         character varying,
    "block_level"                        float8,
    "block_timestamp"                    character varying,
    "branch"                             character varying,
    "double_endorsement_evidences_uuid"  character varying,
    "kind"                               character varying,
    "level"                              float8,
    "signature"                          character varying,
    "uuid"                               character varying unique);
CREATE INDEX "op1s_block_alpha_cycle_idx" ON op1s ("block_alpha_cycle");
CREATE INDEX "op1s_block_hash_idx" ON op1s ("block_hash");
CREATE INDEX "op1s_block_level_idx" ON op1s ("block_level");
CREATE INDEX "op1s_block_timestamp_idx" ON op1s ("block_timestamp");
CREATE INDEX "op1s_branch_idx" ON op1s ("branch");
CREATE INDEX "op1s_double_endorsement_evidences_uuid_idx" ON op1s ("double_endorsement_evidences_uuid");
CREATE INDEX "op1s_kind_idx" ON op1s ("kind");
CREATE INDEX "op1s_level_idx" ON op1s ("level");
CREATE INDEX "op1s_signature_idx" ON op1s ("signature");
CREATE INDEX "op1s_uuid_idx" ON op1s ("uuid");

CREATE TABLE bh1s (
    "_inserted_timestamp"           timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"             float8,
    "block_hash"                    character varying,
    "block_level"                   float8,
    "block_timestamp"               character varying,
    "context"                       character varying,
    "double_baking_evidences_uuid"  character varying,
    "fitness"                       character varying[]
       DEFAULT '{}',
    "level"                         float8,
    "operations_hash"               character varying,
    "predecessor"                   character varying,
    "priority"                      float8,
    "proof_of_work_nonce"           character varying,
    "proto"                         float8,
    "signature"                     character varying,
    "timestamp"                     character varying,
    "uuid"                          character varying unique,
    "validation_pass"               float8);
CREATE INDEX "bh1s_block_alpha_cycle_idx" ON bh1s ("block_alpha_cycle");
CREATE INDEX "bh1s_block_hash_idx" ON bh1s ("block_hash");
CREATE INDEX "bh1s_block_level_idx" ON bh1s ("block_level");
CREATE INDEX "bh1s_block_timestamp_idx" ON bh1s ("block_timestamp");
CREATE INDEX "bh1s_context_idx" ON bh1s ("context");
CREATE INDEX "bh1s_double_baking_evidences_uuid_idx" ON bh1s ("double_baking_evidences_uuid");
CREATE INDEX "bh1s_fitness_idx" ON bh1s ("fitness");
CREATE INDEX "bh1s_level_idx" ON bh1s ("level");
CREATE INDEX "bh1s_operations_hash_idx" ON bh1s ("operations_hash");
CREATE INDEX "bh1s_predecessor_idx" ON bh1s ("predecessor");
CREATE INDEX "bh1s_priority_idx" ON bh1s ("priority");
CREATE INDEX "bh1s_proof_of_work_nonce_idx" ON bh1s ("proof_of_work_nonce");
CREATE INDEX "bh1s_proto_idx" ON bh1s ("proto");
CREATE INDEX "bh1s_signature_idx" ON bh1s ("signature");
CREATE INDEX "bh1s_timestamp_idx" ON bh1s ("timestamp");
CREATE INDEX "bh1s_uuid_idx" ON bh1s ("uuid");
CREATE INDEX "bh1s_validation_pass_idx" ON bh1s ("validation_pass");

CREATE TABLE op_double_endorsement_evidences (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"    float8,
    "block_hash"           character varying,
    "block_level"          float8,
    "block_timestamp"      character varying,
    "kind"                 character varying,
    "metadata_uuid"        character varying,
    "op_uuid"              character varying,
    "uuid"                 character varying unique);
CREATE INDEX "op_double_endorsement_evidences_block_alpha_cycle_idx" ON op_double_endorsement_evidences ("block_alpha_cycle");
CREATE INDEX "op_double_endorsement_evidences_block_hash_idx" ON op_double_endorsement_evidences ("block_hash");
CREATE INDEX "op_double_endorsement_evidences_block_level_idx" ON op_double_endorsement_evidences ("block_level");
CREATE INDEX "op_double_endorsement_evidences_block_timestamp_idx" ON op_double_endorsement_evidences ("block_timestamp");
CREATE INDEX "op_double_endorsement_evidences_kind_idx" ON op_double_endorsement_evidences ("kind");
CREATE INDEX "op_double_endorsement_evidences_metadata_uuid_idx" ON op_double_endorsement_evidences ("metadata_uuid");
CREATE INDEX "op_double_endorsement_evidences_op_uuid_idx" ON op_double_endorsement_evidences ("op_uuid");
CREATE INDEX "op_double_endorsement_evidences_uuid_idx" ON op_double_endorsement_evidences ("uuid");

CREATE TABLE bh2s (
    "_inserted_timestamp"           timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"             float8,
    "block_hash"                    character varying,
    "block_level"                   float8,
    "block_timestamp"               character varying,
    "context"                       character varying,
    "double_baking_evidences_uuid"  character varying,
    "fitness"                       character varying[]
       DEFAULT '{}',
    "level"                         float8,
    "operations_hash"               character varying,
    "predecessor"                   character varying,
    "priority"                      float8,
    "proof_of_work_nonce"           character varying,
    "proto"                         float8,
    "signature"                     character varying,
    "timestamp"                     character varying,
    "uuid"                          character varying unique,
    "validation_pass"               float8);
CREATE INDEX "bh2s_block_alpha_cycle_idx" ON bh2s ("block_alpha_cycle");
CREATE INDEX "bh2s_block_hash_idx" ON bh2s ("block_hash");
CREATE INDEX "bh2s_block_level_idx" ON bh2s ("block_level");
CREATE INDEX "bh2s_block_timestamp_idx" ON bh2s ("block_timestamp");
CREATE INDEX "bh2s_context_idx" ON bh2s ("context");
CREATE INDEX "bh2s_double_baking_evidences_uuid_idx" ON bh2s ("double_baking_evidences_uuid");
CREATE INDEX "bh2s_fitness_idx" ON bh2s ("fitness");
CREATE INDEX "bh2s_level_idx" ON bh2s ("level");
CREATE INDEX "bh2s_operations_hash_idx" ON bh2s ("operations_hash");
CREATE INDEX "bh2s_predecessor_idx" ON bh2s ("predecessor");
CREATE INDEX "bh2s_priority_idx" ON bh2s ("priority");
CREATE INDEX "bh2s_proof_of_work_nonce_idx" ON bh2s ("proof_of_work_nonce");
CREATE INDEX "bh2s_proto_idx" ON bh2s ("proto");
CREATE INDEX "bh2s_signature_idx" ON bh2s ("signature");
CREATE INDEX "bh2s_timestamp_idx" ON bh2s ("timestamp");
CREATE INDEX "bh2s_uuid_idx" ON bh2s ("uuid");
CREATE INDEX "bh2s_validation_pass_idx" ON bh2s ("validation_pass");

CREATE TABLE block_alphas (
    "_inserted_timestamp"        timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "baker"                      character varying,
    "block_uuid"                 character varying,
    "consumed_gas"               character varying,
    "cycle"                      float8,
    "cycle_position"             float8,
    "expected_commitment"        bool,
    "level"                      float8,
    "level_position"             float8,
    "max_block_header_length"    float8,
    "max_operation_data_length"  float8,
    "max_operations_ttl"         float8,
    "next_protocol"              character varying,
    "nonce_hash"                 character varying,
    "protocol"                   character varying,
    "test_chain_chain_id"        character varying,
    "test_chain_expiration"      character varying,
    "test_chain_genesis"         character varying,
    "test_chain_parent_uuid"     character varying,
    "test_chain_protocol"        character varying,
    "test_chain_status"          character varying,
    "uuid"                       character varying unique,
    "voting_period"              float8,
    "voting_period_kind"         character varying,
    "voting_period_position"     float8);
CREATE INDEX "block_alphas_baker_idx" ON block_alphas ("baker");
CREATE INDEX "block_alphas_block_uuid_idx" ON block_alphas ("block_uuid");
CREATE INDEX "block_alphas_consumed_gas_idx" ON block_alphas ("consumed_gas");
CREATE INDEX "block_alphas_cycle_idx" ON block_alphas ("cycle");
CREATE INDEX "block_alphas_cycle_position_idx" ON block_alphas ("cycle_position");
CREATE INDEX "block_alphas_expected_commitment_idx" ON block_alphas ("expected_commitment");
CREATE INDEX "block_alphas_level_idx" ON block_alphas ("level");
CREATE INDEX "block_alphas_level_position_idx" ON block_alphas ("level_position");
CREATE INDEX "block_alphas_max_block_header_length_idx" ON block_alphas ("max_block_header_length");
CREATE INDEX "block_alphas_max_operation_data_length_idx" ON block_alphas ("max_operation_data_length");
CREATE INDEX "block_alphas_max_operations_ttl_idx" ON block_alphas ("max_operations_ttl");
CREATE INDEX "block_alphas_next_protocol_idx" ON block_alphas ("next_protocol");
CREATE INDEX "block_alphas_nonce_hash_idx" ON block_alphas ("nonce_hash");
CREATE INDEX "block_alphas_protocol_idx" ON block_alphas ("protocol");
CREATE INDEX "block_alphas_test_chain_chain_id_idx" ON block_alphas ("test_chain_chain_id");
CREATE INDEX "block_alphas_test_chain_expiration_idx" ON block_alphas ("test_chain_expiration");
CREATE INDEX "block_alphas_test_chain_genesis_idx" ON block_alphas ("test_chain_genesis");
CREATE INDEX "block_alphas_test_chain_parent_uuid_idx" ON block_alphas ("test_chain_parent_uuid");
CREATE INDEX "block_alphas_test_chain_protocol_idx" ON block_alphas ("test_chain_protocol");
CREATE INDEX "block_alphas_test_chain_status_idx" ON block_alphas ("test_chain_status");
CREATE INDEX "block_alphas_uuid_idx" ON block_alphas ("uuid");
CREATE INDEX "block_alphas_voting_period_idx" ON block_alphas ("voting_period");
CREATE INDEX "block_alphas_voting_period_kind_idx" ON block_alphas ("voting_period_kind");
CREATE INDEX "block_alphas_voting_period_position_idx" ON block_alphas ("voting_period_position");

CREATE TABLE op_delegations (
    "_inserted_timestamp"                      timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "balance"                                  character varying,
    "block_alpha_cycle"                        float8,
    "block_hash"                               character varying,
    "block_level"                              float8,
    "block_timestamp"                          character varying,
    "counter"                                  character varying,
    "delegate"                                 character varying,
    "fee"                                      character varying,
    "gas_limit"                                character varying,
    "kind"                                     character varying,
    "metadata_uuid"                            character varying,
    "op_uuid"                                  character varying,
    "operation_result_big_map_diff"            character varying,
    "operation_result_consumed_gas"            character varying,
    "operation_result_errors"                  character varying,
    "operation_result_originated_contracts"    character varying[]
       DEFAULT '{}',
    "operation_result_paid_storage_size_diff"  character varying,
    "operation_result_status"                  character varying,
    "operation_result_storage_size"            character varying,
    "operation_result_uuid"                    character varying,
    "script"                                   character varying,
    "source"                                   character varying,
    "storage_limit"                            character varying,
    "uuid"                                     character varying unique);
CREATE INDEX "op_delegations_balance_idx" ON op_delegations ("balance");
CREATE INDEX "op_delegations_block_alpha_cycle_idx" ON op_delegations ("block_alpha_cycle");
CREATE INDEX "op_delegations_block_hash_idx" ON op_delegations ("block_hash");
CREATE INDEX "op_delegations_block_level_idx" ON op_delegations ("block_level");
CREATE INDEX "op_delegations_block_timestamp_idx" ON op_delegations ("block_timestamp");
CREATE INDEX "op_delegations_counter_idx" ON op_delegations ("counter");
CREATE INDEX "op_delegations_delegate_idx" ON op_delegations ("delegate");
CREATE INDEX "op_delegations_fee_idx" ON op_delegations ("fee");
CREATE INDEX "op_delegations_gas_limit_idx" ON op_delegations ("gas_limit");
CREATE INDEX "op_delegations_kind_idx" ON op_delegations ("kind");
CREATE INDEX "op_delegations_metadata_uuid_idx" ON op_delegations ("metadata_uuid");
CREATE INDEX "op_delegations_op_uuid_idx" ON op_delegations ("op_uuid");
CREATE INDEX "op_delegations_operation_result_consumed_gas_idx" ON op_delegations ("operation_result_consumed_gas");
CREATE INDEX "op_delegations_operation_result_originated_contracts_idx" ON op_delegations ("operation_result_originated_contracts");
CREATE INDEX "op_delegations_operation_result_paid_storage_size_diff_idx" ON op_delegations ("operation_result_paid_storage_size_diff");
CREATE INDEX "op_delegations_operation_result_status_idx" ON op_delegations ("operation_result_status");
CREATE INDEX "op_delegations_operation_result_storage_size_idx" ON op_delegations ("operation_result_storage_size");
CREATE INDEX "op_delegations_operation_result_uuid_idx" ON op_delegations ("operation_result_uuid");
CREATE INDEX "op_delegations_source_idx" ON op_delegations ("source");
CREATE INDEX "op_delegations_storage_limit_idx" ON op_delegations ("storage_limit");
CREATE INDEX "op_delegations_uuid_idx" ON op_delegations ("uuid");

CREATE TABLE internal_op_delegations (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "delegate"             character varying,
    "kind"                 character varying,
    "nonce"                float8,
    "op_metadata_uuid"     character varying,
    "result_consumed_gas"  character varying,
    "result_errors"        character varying,
    "result_status"        character varying,
    "result_uuid"          character varying,
    "source"               character varying,
    "uuid"                 character varying unique);
CREATE INDEX "internal_op_delegations_delegate_idx" ON internal_op_delegations ("delegate");
CREATE INDEX "internal_op_delegations_kind_idx" ON internal_op_delegations ("kind");
CREATE INDEX "internal_op_delegations_nonce_idx" ON internal_op_delegations ("nonce");
CREATE INDEX "internal_op_delegations_op_metadata_uuid_idx" ON internal_op_delegations ("op_metadata_uuid");
CREATE INDEX "internal_op_delegations_result_consumed_gas_idx" ON internal_op_delegations ("result_consumed_gas");
CREATE INDEX "internal_op_delegations_result_status_idx" ON internal_op_delegations ("result_status");
CREATE INDEX "internal_op_delegations_result_uuid_idx" ON internal_op_delegations ("result_uuid");
CREATE INDEX "internal_op_delegations_source_idx" ON internal_op_delegations ("source");
CREATE INDEX "internal_op_delegations_uuid_idx" ON internal_op_delegations ("uuid");

CREATE TABLE internal_op_txs (
    "_inserted_timestamp"                    timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "amount"                                 character varying,
    "block_alpha_cycle"                      float8,
    "block_hash"                             character varying,
    "block_level"                            float8,
    "block_timestamp"                        character varying,
    "destination"                            character varying,
    "kind"                                   character varying,
    "nonce"                                  float8,
    "op_metadata_uuid"                       character varying,
    "parameters"                             character varying,
    "result_allocated_destination_contract"  bool,
    "result_big_map_diff"                    character varying,
    "result_consumed_gas"                    character varying,
    "result_errors"                          character varying,
    "result_paid_storage_size_diff"          character varying,
    "result_status"                          character varying,
    "result_storage"                         character varying,
    "result_storage_size"                    character varying,
    "result_uuid"                            character varying,
    "source"                                 character varying,
    "uuid"                                   character varying unique);
CREATE INDEX "internal_op_txs_amount_idx" ON internal_op_txs ("amount");
CREATE INDEX "internal_op_txs_block_alpha_cycle_idx" ON internal_op_txs ("block_alpha_cycle");
CREATE INDEX "internal_op_txs_block_hash_idx" ON internal_op_txs ("block_hash");
CREATE INDEX "internal_op_txs_block_level_idx" ON internal_op_txs ("block_level");
CREATE INDEX "internal_op_txs_block_timestamp_idx" ON internal_op_txs ("block_timestamp");
CREATE INDEX "internal_op_txs_destination_idx" ON internal_op_txs ("destination");
CREATE INDEX "internal_op_txs_kind_idx" ON internal_op_txs ("kind");
CREATE INDEX "internal_op_txs_nonce_idx" ON internal_op_txs ("nonce");
CREATE INDEX "internal_op_txs_op_metadata_uuid_idx" ON internal_op_txs ("op_metadata_uuid");
CREATE INDEX "internal_op_txs_result_allocated_destination_contract_idx" ON internal_op_txs ("result_allocated_destination_contract");
CREATE INDEX "internal_op_txs_result_consumed_gas_idx" ON internal_op_txs ("result_consumed_gas");
CREATE INDEX "internal_op_txs_result_paid_storage_size_diff_idx" ON internal_op_txs ("result_paid_storage_size_diff");
CREATE INDEX "internal_op_txs_result_status_idx" ON internal_op_txs ("result_status");
CREATE INDEX "internal_op_txs_result_storage_size_idx" ON internal_op_txs ("result_storage_size");
CREATE INDEX "internal_op_txs_result_uuid_idx" ON internal_op_txs ("result_uuid");
CREATE INDEX "internal_op_txs_source_idx" ON internal_op_txs ("source");
CREATE INDEX "internal_op_txs_uuid_idx" ON internal_op_txs ("uuid");

CREATE TABLE op_txs (
    "_inserted_timestamp"                              timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "amount"                                           character varying,
    "block_alpha_cycle"                                float8,
    "block_hash"                                       character varying,
    "block_level"                                      float8,
    "block_timestamp"                                  character varying,
    "counter"                                          character varying,
    "destination"                                      character varying,
    "fee"                                              character varying,
    "gas_limit"                                        character varying,
    "kind"                                             character varying,
    "metadata_uuid"                                    character varying,
    "op_uuid"                                          character varying,
    "operation_result_allocated_destination_contract"  bool,
    "operation_result_big_map_diff"                    character varying,
    "operation_result_consumed_gas"                    character varying,
    "operation_result_errors"                          character varying,
    "operation_result_paid_storage_size_diff"          character varying,
    "operation_result_status"                          character varying,
    "operation_result_storage"                         character varying,
    "operation_result_storage_size"                    character varying,
    "operation_result_uuid"                            character varying,
    "parameters"                                       character varying,
    "source"                                           character varying,
    "storage_limit"                                    character varying,
    "uuid"                                             character varying unique);
CREATE INDEX "op_txs_amount_idx" ON op_txs ("amount");
CREATE INDEX "op_txs_block_alpha_cycle_idx" ON op_txs ("block_alpha_cycle");
CREATE INDEX "op_txs_block_hash_idx" ON op_txs ("block_hash");
CREATE INDEX "op_txs_block_level_idx" ON op_txs ("block_level");
CREATE INDEX "op_txs_block_timestamp_idx" ON op_txs ("block_timestamp");
CREATE INDEX "op_txs_counter_idx" ON op_txs ("counter");
CREATE INDEX "op_txs_destination_idx" ON op_txs ("destination");
CREATE INDEX "op_txs_fee_idx" ON op_txs ("fee");
CREATE INDEX "op_txs_gas_limit_idx" ON op_txs ("gas_limit");
CREATE INDEX "op_txs_kind_idx" ON op_txs ("kind");
CREATE INDEX "op_txs_metadata_uuid_idx" ON op_txs ("metadata_uuid");
CREATE INDEX "op_txs_op_uuid_idx" ON op_txs ("op_uuid");
CREATE INDEX "op_txs_operation_result_allocated_destination_contract_idx" ON op_txs ("operation_result_allocated_destination_contract");
CREATE INDEX "op_txs_operation_result_consumed_gas_idx" ON op_txs ("operation_result_consumed_gas");
CREATE INDEX "op_txs_operation_result_paid_storage_size_diff_idx" ON op_txs ("operation_result_paid_storage_size_diff");
CREATE INDEX "op_txs_operation_result_status_idx" ON op_txs ("operation_result_status");
CREATE INDEX "op_txs_operation_result_storage_size_idx" ON op_txs ("operation_result_storage_size");
CREATE INDEX "op_txs_operation_result_uuid_idx" ON op_txs ("operation_result_uuid");
CREATE INDEX "op_txs_source_idx" ON op_txs ("source");
CREATE INDEX "op_txs_storage_limit_idx" ON op_txs ("storage_limit");
CREATE INDEX "op_txs_uuid_idx" ON op_txs ("uuid");

CREATE TABLE op_originations (
    "_inserted_timestamp"                      timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "balance"                                  character varying,
    "block_alpha_cycle"                        float8,
    "block_hash"                               character varying,
    "block_level"                              float8,
    "block_timestamp"                          character varying,
    "counter"                                  character varying,
    "delegatable"                              bool,
    "delegate"                                 character varying,
    "fee"                                      character varying,
    "gas_limit"                                character varying,
    "kind"                                     character varying,
    "manager_pubkey"                           character varying,
    "metadata_uuid"                            character varying,
    "op_uuid"                                  character varying,
    "operation_result_consumed_gas"            character varying,
    "operation_result_errors"                  character varying,
    "operation_result_originated_contracts"    character varying[]
       DEFAULT '{}',
    "operation_result_paid_storage_size_diff"  character varying,
    "operation_result_status"                  character varying,
    "operation_result_storage_size"            character varying,
    "operation_result_uuid"                    character varying,
    "script"                                   character varying,
    "source"                                   character varying,
    "spendable"                                bool,
    "storage_limit"                            character varying,
    "uuid"                                     character varying unique);
CREATE INDEX "op_originations_balance_idx" ON op_originations ("balance");
CREATE INDEX "op_originations_block_alpha_cycle_idx" ON op_originations ("block_alpha_cycle");
CREATE INDEX "op_originations_block_hash_idx" ON op_originations ("block_hash");
CREATE INDEX "op_originations_block_level_idx" ON op_originations ("block_level");
CREATE INDEX "op_originations_block_timestamp_idx" ON op_originations ("block_timestamp");
CREATE INDEX "op_originations_counter_idx" ON op_originations ("counter");
CREATE INDEX "op_originations_delegatable_idx" ON op_originations ("delegatable");
CREATE INDEX "op_originations_delegate_idx" ON op_originations ("delegate");
CREATE INDEX "op_originations_fee_idx" ON op_originations ("fee");
CREATE INDEX "op_originations_gas_limit_idx" ON op_originations ("gas_limit");
CREATE INDEX "op_originations_kind_idx" ON op_originations ("kind");
CREATE INDEX "op_originations_manager_pubkey_idx" ON op_originations ("manager_pubkey");
CREATE INDEX "op_originations_metadata_uuid_idx" ON op_originations ("metadata_uuid");
CREATE INDEX "op_originations_op_uuid_idx" ON op_originations ("op_uuid");
CREATE INDEX "op_originations_operation_result_consumed_gas_idx" ON op_originations ("operation_result_consumed_gas");
CREATE INDEX "op_originations_operation_result_originated_contracts_idx" ON op_originations ("operation_result_originated_contracts");
CREATE INDEX "op_originations_operation_result_paid_storage_size_diff_idx" ON op_originations ("operation_result_paid_storage_size_diff");
CREATE INDEX "op_originations_operation_result_status_idx" ON op_originations ("operation_result_status");
CREATE INDEX "op_originations_operation_result_storage_size_idx" ON op_originations ("operation_result_storage_size");
CREATE INDEX "op_originations_operation_result_uuid_idx" ON op_originations ("operation_result_uuid");
CREATE INDEX "op_originations_source_idx" ON op_originations ("source");
CREATE INDEX "op_originations_spendable_idx" ON op_originations ("spendable");
CREATE INDEX "op_originations_storage_limit_idx" ON op_originations ("storage_limit");
CREATE INDEX "op_originations_uuid_idx" ON op_originations ("uuid");

CREATE TABLE op_double_baking_evidences (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"    float8,
    "block_hash"           character varying,
    "block_level"          float8,
    "block_timestamp"      character varying,
    "kind"                 character varying,
    "metadata_uuid"        character varying,
    "op_uuid"              character varying,
    "uuid"                 character varying unique);
CREATE INDEX "op_double_baking_evidences_block_alpha_cycle_idx" ON op_double_baking_evidences ("block_alpha_cycle");
CREATE INDEX "op_double_baking_evidences_block_hash_idx" ON op_double_baking_evidences ("block_hash");
CREATE INDEX "op_double_baking_evidences_block_level_idx" ON op_double_baking_evidences ("block_level");
CREATE INDEX "op_double_baking_evidences_block_timestamp_idx" ON op_double_baking_evidences ("block_timestamp");
CREATE INDEX "op_double_baking_evidences_kind_idx" ON op_double_baking_evidences ("kind");
CREATE INDEX "op_double_baking_evidences_metadata_uuid_idx" ON op_double_baking_evidences ("metadata_uuid");
CREATE INDEX "op_double_baking_evidences_op_uuid_idx" ON op_double_baking_evidences ("op_uuid");
CREATE INDEX "op_double_baking_evidences_uuid_idx" ON op_double_baking_evidences ("uuid");

CREATE TABLE op_seed_nonce_revelations (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"    float8,
    "block_hash"           character varying,
    "block_level"          float8,
    "block_timestamp"      character varying,
    "kind"                 character varying,
    "level"                float8,
    "metadata_uuid"        character varying,
    "nonce"                character varying,
    "op_uuid"              character varying,
    "uuid"                 character varying unique);
CREATE INDEX "op_seed_nonce_revelations_block_alpha_cycle_idx" ON op_seed_nonce_revelations ("block_alpha_cycle");
CREATE INDEX "op_seed_nonce_revelations_block_hash_idx" ON op_seed_nonce_revelations ("block_hash");
CREATE INDEX "op_seed_nonce_revelations_block_level_idx" ON op_seed_nonce_revelations ("block_level");
CREATE INDEX "op_seed_nonce_revelations_block_timestamp_idx" ON op_seed_nonce_revelations ("block_timestamp");
CREATE INDEX "op_seed_nonce_revelations_kind_idx" ON op_seed_nonce_revelations ("kind");
CREATE INDEX "op_seed_nonce_revelations_level_idx" ON op_seed_nonce_revelations ("level");
CREATE INDEX "op_seed_nonce_revelations_metadata_uuid_idx" ON op_seed_nonce_revelations ("metadata_uuid");
CREATE INDEX "op_seed_nonce_revelations_nonce_idx" ON op_seed_nonce_revelations ("nonce");
CREATE INDEX "op_seed_nonce_revelations_op_uuid_idx" ON op_seed_nonce_revelations ("op_uuid");
CREATE INDEX "op_seed_nonce_revelations_uuid_idx" ON op_seed_nonce_revelations ("uuid");

CREATE TABLE op_ballots (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "ballot"               character varying,
    "block_alpha_cycle"    float8,
    "block_hash"           character varying,
    "block_level"          float8,
    "block_timestamp"      character varying,
    "kind"                 character varying,
    "metadata_uuid"        character varying,
    "op_uuid"              character varying,
    "period"               float8,
    "proposal"             character varying,
    "source"               character varying,
    "uuid"                 character varying unique);
CREATE INDEX "op_ballots_ballot_idx" ON op_ballots ("ballot");
CREATE INDEX "op_ballots_block_alpha_cycle_idx" ON op_ballots ("block_alpha_cycle");
CREATE INDEX "op_ballots_block_hash_idx" ON op_ballots ("block_hash");
CREATE INDEX "op_ballots_block_level_idx" ON op_ballots ("block_level");
CREATE INDEX "op_ballots_block_timestamp_idx" ON op_ballots ("block_timestamp");
CREATE INDEX "op_ballots_kind_idx" ON op_ballots ("kind");
CREATE INDEX "op_ballots_metadata_uuid_idx" ON op_ballots ("metadata_uuid");
CREATE INDEX "op_ballots_op_uuid_idx" ON op_ballots ("op_uuid");
CREATE INDEX "op_ballots_period_idx" ON op_ballots ("period");
CREATE INDEX "op_ballots_proposal_idx" ON op_ballots ("proposal");
CREATE INDEX "op_ballots_source_idx" ON op_ballots ("source");
CREATE INDEX "op_ballots_uuid_idx" ON op_ballots ("uuid");

CREATE TABLE op2s (
    "_inserted_timestamp"                timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "block_alpha_cycle"                  float8,
    "block_hash"                         character varying,
    "block_level"                        float8,
    "block_timestamp"                    character varying,
    "branch"                             character varying,
    "double_endorsement_evidences_uuid"  character varying,
    "kind"                               character varying,
    "level"                              float8,
    "signature"                          character varying,
    "uuid"                               character varying unique);
CREATE INDEX "op2s_block_alpha_cycle_idx" ON op2s ("block_alpha_cycle");
CREATE INDEX "op2s_block_hash_idx" ON op2s ("block_hash");
CREATE INDEX "op2s_block_level_idx" ON op2s ("block_level");
CREATE INDEX "op2s_block_timestamp_idx" ON op2s ("block_timestamp");
CREATE INDEX "op2s_branch_idx" ON op2s ("branch");
CREATE INDEX "op2s_double_endorsement_evidences_uuid_idx" ON op2s ("double_endorsement_evidences_uuid");
CREATE INDEX "op2s_kind_idx" ON op2s ("kind");
CREATE INDEX "op2s_level_idx" ON op2s ("level");
CREATE INDEX "op2s_signature_idx" ON op2s ("signature");
CREATE INDEX "op2s_uuid_idx" ON op2s ("uuid");

