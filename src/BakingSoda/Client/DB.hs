{-# LANGUAGE FlexibleContexts           #-}

module BakingSoda.Client.DB where

import qualified Data.Pool as P

import Conduit

import Control.Concurrent
import Control.Monad
import Control.Monad.IO.Class  (liftIO, MonadIO)
import Control.Monad.Reader
import Control.Monad.Trans.Control

import Database.Groundhog as G
import Database.Groundhog.Core as G
import Database.Groundhog.Generic as G
import Database.Groundhog.Generic.Sql as G
import Database.Groundhog.Postgresql as G
import Database.Groundhog.Postgresql.Array as G

import System.IO

blockStatTs = [ "op_txs"
              , "op_delegations"
              , "op_originations"
              , "op_reveals"
              ]

simpleUpdateBlockInfosBHead
  :: String
  -> String
  -> IO ()
simpleUpdateBlockInfosBHead cs csr = do
    pool <- createPostgresqlPool cs 3
    poolRead <- createPostgresqlPool csr 3

    simpleUpdateBlockStatFromWait pool poolRead blockStatTs blockStatTs

simpleUpdateBlockStatOpFrom
    :: (MonadIO m, MonadUnliftIO m, MonadBaseControl IO m)
    => P.Pool Postgresql
    -> P.Pool Postgresql
    -> m ()
simpleUpdateBlockStatOpFrom pool poolRead = do
    liftIO $ putStrLn "[ block_stats op ] postprocessing ...  "
    let uQueryOCount  = "update block_stats"
          <> " set ops_count = s.count"
          <> " from (select bs.level as level,count(block_level) as count"
             <> " from block_stats as bs"
             <> " left join ops on ops.block_level = bs.level"
             <> " where bs.ops_count is null"
             <> " or bs.level > (select max(level) from block_stats) - 500"
             <> " group by bs.level) as s"
          <> " where block_stats.level = s.level;"
        uQueryAmount = "update block_stats"
          <> " set op_txs_volume = s.amount"
          <> " from (select bs.level as level,"
            <> " coalesce(sum(cast(op_txs.amount as bigint)), 0) as amount"
            <> " from block_stats as bs"
            <> " left join op_txs on op_txs.block_level = bs.level"
            <> " where bs.op_txs_volume is null"
            <> " or bs.level > (select max(level) from block_stats) - 500"
            <> " group by bs.level) as s"
          <> " where block_stats.level = s.level;"

        uQueries = uQueryAmount <> uQueryOCount
        params = []

    flip runDbConn pool
      $ executeRaw False uQueries params
    liftIO $ threadDelay 1000
    liftIO $ hFlush stdout
    return ()

simpleUpdateBlockStatFromWait
    :: (MonadIO m, MonadUnliftIO m, MonadBaseControl IO m)
    => P.Pool Postgresql
    -> P.Pool Postgresql
    -> [ String ] -- ^ updated fee
    -> [ String ] -- ^ updated count
    -> m ()
simpleUpdateBlockStatFromWait pool poolRead upF upC = do
    simpleUpdateBlockStatFrom pool poolRead upF upC
    simpleUpdateBlockStatOpFrom pool poolRead
    simpleUpdateBlockStatFromWait pool poolRead upF upC

simpleUpdateBlockStatFrom
    :: (MonadIO m, MonadUnliftIO m, MonadBaseControl IO m)
    => P.Pool Postgresql
    -> P.Pool Postgresql
    -> [ String ] -- ^ updated fee
    -> [ String ] -- ^ updated count
    -> m ()
simpleUpdateBlockStatFrom pool poolRead upF upC = do
    liftIO $ putStrLn "[ block_stats ] postprocessing ...  "
    let uQueryFee t = "update block_stats"
          <> " set " <> t <> "_fee = s.fee"
          <> " from (select bs.level as level,"
            <> " coalesce(sum(cast(" <> t <> ".fee as bigint)), 0) as fee"
            <> " from block_stats as bs"
            <> " left join " <> t <> " on " <> t <> ".block_level = bs.level"
            <> " where bs." <> t <> "_fee is null"
            <> " or bs.level > (select max(level) from block_stats) - 500"
            <> " group by bs.level) as s"
          <> " where block_stats.level = s.level;"
        uQueryCount t = "update block_stats"
          <> " set " <> t <> "_count = s.count"
          <> " from (select bs.level as level,count(block_level) as count"
             <> " from block_stats as bs"
             <> " left join " <> t <> " on " <> t <> ".block_level = bs.level"
             <> " where bs." <> t <> "_count is null"
             <> " or bs.level > (select max(level) from block_stats) - 500"
             <> " group by bs.level) as s"
          <> " where block_stats.level = s.level;"

        uQueriesF = unwords $ map uQueryFee upF
        uQueriesC = unwords $ map uQueryCount upC
        uQueries = uQueriesF <> uQueriesC
        params = []

    flip runDbConn pool
      $ executeRaw False uQueries params
    liftIO $ threadDelay 1000
    liftIO $ hFlush stdout
    return ()

toNum = fromInteger . toInteger

raw_ :: (PersistBackend m, MonadIO m, MonadBaseControl IO m)
     => String
     -> [ G.PersistValue ]
     -> ([[PersistValue]] -> m r)
     -> m r
raw_ s x f = do
         q <- G.queryRaw False s x
         rawValues <- G.streamToList q

         f rawValues
