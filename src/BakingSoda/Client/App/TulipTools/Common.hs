{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings  #-}

module BakingSoda.Client.App.TulipTools.Common where

import Data.Typeable
import Data.Data
import Data.Char
import qualified Data.HashMap.Strict as H
import qualified Data.Time.Clock as TC

import BakingSoda.Crawler
import BakingSoda.Model
import BakingSoda.Client.App.Common

---
{-
- Define TulipToolsCommon Crawler
-}

data TulipToolsCommon = TTT
                  { time :: TC.UTCTime }
             deriving (Data, Typeable, Show, Read, Eq)

instance Iso TulipToolsCommon where
    zero = id
    succ ttt = TTT $ TC.addUTCTime tenMins (time ttt)

tenMins :: TC.NominalDiffTime
tenMins = TC.nominalDay / 1440 * 10

sleep10mins :: Int
sleep10mins = floor $ toRational tenMins * 1000000

runTulipToolsCommonCrawler :: String -> String -> String -> String -> String -> Int -> Int -> Int -> Int -> String -> IO ()
runTulipToolsCommonCrawler name url id cs csr n t d1 d2 _ = do
    putStrLn $ "starting to run " <> name <> " crawler.."
    currentTime <- TC.getCurrentTime
    let ttt = TTT { time = currentTime }
    storeSnapshot 3 (Gain (map toLower name) ttt url) n t sleep10mins d2 id cs csr

---
{-
- Define TulipToolsCommon Model
-}

stopRule :: [ Rule () ]
stopRule = []

buildTulipToolsCommonModel :: Storage -> Int -> Int
  -> String -> String -> String -> String -> String -> IO ()
buildTulipToolsCommonModel s n t = buildModel n 0 t s stopRule

---
{-
- Parser Block Json
-}
parserTulipToolsCommonBlockJson :: String -> String -> String -> String -> String -> Int -> Int -> Int -> Int -> Int -> IO ()
parserTulipToolsCommonBlockJson = parserJson stopRule


