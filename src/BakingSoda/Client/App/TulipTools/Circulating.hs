{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings  #-}

module BakingSoda.Client.App.TulipTools.Circulating where

import Data.Typeable
import Data.Data
import qualified Data.HashMap.Strict as H
import qualified Data.Time.Clock as TC

import BakingSoda.Crawler
import BakingSoda.Model
import BakingSoda.Client.App.Common
import BakingSoda.Client.App.TulipTools.Common

tuliptoolsCirculating :: String
tuliptoolsCirculating = "https://cmc.tulip.tools/circulating"

runTulipToolsCirculatingCrawler :: String -> String -> String -> Int -> Int -> Int -> Int -> String -> IO ()
runTulipToolsCirculatingCrawler =
    runTulipToolsCommonCrawler  "TulipToolsCirculating" tuliptoolsCirculating

buildTulipToolsCirculatingModel :: Int -> Int
  -> String -> String -> String -> String -> String -> IO ()
buildTulipToolsCirculatingModel =
    buildTulipToolsCommonModel (DS "tulip_tools" "tulip_tools_circulating")
