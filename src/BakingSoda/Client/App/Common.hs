{-# LANGUAGE OverloadedStrings  #-}

module BakingSoda.Client.App.Common where

import qualified Data.HashMap.Strict as H

import BakingSoda.Model as BS
import BakingSoda.Parser as BS
import BakingSoda.Gen as BS
import BakingSoda.Model as BS

buildModel
  :: Int         -- ^ total
  -> Int         -- ^ page
  -> Int         -- ^ number
  -> Storage     -- ^ default storage
  -> [ Rule () ] -- ^ stop rule
  -> String      -- ^ source
  -> String      -- ^ connect string
  -> String      -- ^ connect string for read
  -> String      -- ^ config file
  -> String      -- ^ model file
  -> IO ()
buildModel to pa nu ds st id cs csr cf env = do
    putStrLn "generating config and building model..."
    let e = Env H.empty
        s = H.empty
    build to pa nu env cf e s ds (Just 10) st id cs csr

verifyConfig :: String -> IO ()
verifyConfig cf = do
    putStrLn "verify config and re-generating..."
    BS.verifyConfig cf

genSql :: String -> IO ()
genSql cf = do
    putStrLn "generating creating table schema..."
    genCreateTableSql cf "./createTable.sql"

genTableHaskellDT :: String -> IO ()
genTableHaskellDT cf =
    BS.genHaskellTable cf "./Data.hs"

parserJson
  :: [ Rule () ] -- ^ stop rule
  -> String      -- ^ source
  -> String      -- ^ connect string
  -> String      -- ^ connect string for read
  -> String      -- ^ config file
  -> String      -- ^ model file
  -> Int         -- ^ page
  -> Int         -- ^ number of record per page
  -> Int         -- ^ trunk for processing
  -> Int         -- ^ sleep when get full data
  -> Int         -- ^ sleep when lost data in this trunc
  -> IO ()
parserJson st id cs csr cf env p n t d1 d2 = do
    putStrLn "parsing..."
    indexing env cf p n t st d1 d2 id cs csr
