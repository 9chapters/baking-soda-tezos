#!/bin/bash

## To get DB log
# docker exec -i tezos_db /bin/bash
# cd /var/lib/postgresql/data/log


docker rm baking-soda-tezos -f

docker run -p 5433:5432 --rm --name baking-soda-tezos -v "$PWD/my-postgres.conf":/etc/postgresql/postgresql.conf -e POSTGRES_PASSWORD=pw -e POSTGRES_DB=tezos -d postgres:10.7 -c 'config_file=/etc/postgresql/postgresql.conf'

##sleep 10
##docker exec -i baking-soda-tezos -Upostgres < ./test_data.sql
