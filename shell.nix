{ghc}:
with (import <nixpkgs> {});

haskell.lib.buildStackProject {
  inherit ghc;
  name = "baking-soda-tezos";
  buildInputs = [ zlib postgresql_10 ];
  PGPASSWORD = builtins.getEnv "PGPASSWORD";
}
