module Main where

import BakingSoda.Client.App.Common
import BakingSoda.Client.App.TezosBlock
import BakingSoda.Client.App.MyTezosBaker
import BakingSoda.Client.App.TulipTools.Total
import BakingSoda.Client.App.TulipTools.Circulating
import BakingSoda.Client.App.TulipTools.Common
import BakingSoda.Client.Arg

import qualified Options.Applicative as O
import qualified Data.Maybe as M

main :: IO ()
main = do
    opts <- O.execParser $ O.info (O.helper <*> arg) O.fullDesc
    let Arg mid mcs mcsr mcf menv mp mn mt md1 md2 c = opts
        id = M.fromMaybe defaultId mid
        cs = M.fromMaybe defaultDBConnectStr mcs
        csr = M.fromMaybe cs mcsr
        cf = M.fromMaybe defaultConfigFile mcf
        env = M.fromMaybe defaultEnvFile menv
        p = M.fromMaybe defaultPage mp
        n = M.fromMaybe defaultNumber mn
        t = M.fromMaybe defaultTrunk mt
        d1 = M.fromMaybe defaultSleepS md1
        d2 = M.fromMaybe defaultSleepF md2
    let crawler =
          case id of
            "tezos" -> runTezosCrawler
            "mytezosbaker" -> runMyTezosBakerCrawler
            "tuliptoolstotal" -> runTulipToolsTotalCrawler
            "tuliptoolscirculating" -> runTulipToolsCirculatingCrawler
        model =
          case id of
            "tezos" -> buildTezosModel
            "mytezosbaker" -> buildMyTezosBakerModel
            "tuliptoolstotal" -> buildTulipToolsTotalModel
            "tuliptoolscirculating" -> buildTulipToolsCirculatingModel
        verify = verifyConfig
        gen = genSql
        parser =
          case id of
            "tezos" -> parserTezosBlockJson
            "mytezosbaker" -> parserMyTezosBakerBlockJson
            "tuliptoolstotal" -> parserTulipToolsCommonBlockJson
            "tuliptoolscirculating" -> parserTulipToolsCommonBlockJson
        postprocess =
          case id of
            "tezos" -> postprocessTezosBlock
            _ -> error "no postprocess for mytezosbaker"

    case c of
      Crawl mtz -> do
        let tz = M.fromMaybe defaultTezosNode mtz
        crawler id cs csr n t d1 d2 tz
      Model -> model n t id cs csr cf env
      Verify -> verify cf
      Gen Nothing -> gen cf
      Gen (Just SQLType) -> gen cf
      Gen (Just HaskellType) -> genTableHaskellDT cf
      Parser -> parser id cs csr cf env p n t d1 d2
      PostprocessDB -> postprocessTezosBlock cs csr
